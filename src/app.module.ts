import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { JogadoresModule } from './jogadores/jogadores.module';

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  // useFindAndModify: false
  // useCreateIndex: true,
};


@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://devSmart:smartDev@smartranking.f9xnj.mongodb.net/ranking?retryWrites=true&w=majority', options),
    JogadoresModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
