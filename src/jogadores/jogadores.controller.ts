import { Controller, Post, Body, Get, Put, Delete, UsePipes, ValidationPipe, Query, Param} from '@nestjs/common';
import { createPlayerDto } from './dtos/create-player.dto';
import { player } from './interface/jogadores.interface';
import { JogadoresService } from './jogadores.service';
import { playerValidation } from './pipes/player-validation';

@Controller('api/v1/jogadores')
export class JogadoresController {

  constructor( private readonly player: JogadoresService) {}
  
  @Post('/new')
  @UsePipes(ValidationPipe)
  async createPlayer(@Body() player: createPlayerDto) {
    await this.player.createPlayer(player);
  }

  @Get('/all')
  async getPlayers(): Promise<player[]> {
    return this.player.getPlayer();
  }

  @Get('/:_id')
  async getPlayerByEmail(@Query('_id') id: number): Promise<player> {
    return this.player.getPlayerByID(id);
  }

  @Put('/updatePlayer/:id')
  async updatePlayer(@Param('id') id: number, @Body() player: player): Promise<player> {
    return this.player.update(id,player);
  }

  @Delete('/deletePlayer/:id')
  async deletePlayer(@Param('id', playerValidation) id: number): Promise<player> {
    return this.player.delete(id);
  }

}
