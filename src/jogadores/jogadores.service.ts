import { Injectable, Logger } from '@nestjs/common';
import { createPlayerDto } from './dtos/create-player.dto';
import { player } from './interface/jogadores.interface';
import { v4 as uuid } from 'uuid';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class JogadoresService {
  private readonly logger = new Logger(JogadoresService.name);

  constructor(@InjectModel('players') private readonly playerModel: Model<player>) {}

  async createPlayer(player: createPlayerDto): Promise<player> {
    this.logger.log(`createPlayerDto: ${player}`);
    const createdPlayer = new this.playerModel(player);
    return await createdPlayer.save();
  }

  async getPlayer(): Promise<player[]>  {
    return this.playerModel.find().exec();
  }

  async getPlayerByID(id: number): Promise<player> { 
    const response = await this.playerModel.findOne({ _id: id });
    return response;
  }

  async update(id: number, newUser: player): Promise<player> { 
    const responseUpdate = await this.playerModel.findOneAndUpdate({_id: id},
      {$set: newUser}).exec();
    return responseUpdate;
  }

  async delete(id: number): Promise<any> { 
    const responseDelete = await this.playerModel.deleteOne({_id: id});
    return responseDelete;
  }
}
