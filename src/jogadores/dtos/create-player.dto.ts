import { IsNotEmpty, IsEmail } from 'class-validator';

export class createPlayerDto {
  
  @IsNotEmpty()
  readonly phoneCel: string;

  @IsEmail()
  readonly email: string;

  @IsNotEmpty()
  readonly name: string;
}